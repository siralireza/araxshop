from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User


class CheckPassword(forms.Form):
    password = forms.CharField(label='Password', max_length=30, widget=forms.PasswordInput)
    confirm_password = forms.CharField(label='Retype Password', max_length=30, widget=forms.PasswordInput)

    def clean(self):
        password = self.cleaned_data.get('password')
        confirm_password = self.cleaned_data.get('confirm_password')
        if not password or not confirm_password:
            raise forms.ValidationError("You must fill both password fields")
        if password != confirm_password:
            raise forms.ValidationError("Your passwords do not match")
        return confirm_password


class Login(forms.Form):
    """
    Base class for authenticating users. Extend this to get a form that accepts
    username/password logins.
    """
    username = forms.CharField(label="Username", max_length=30)
    password = forms.CharField(label="Password", widget=forms.PasswordInput)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        if username and password:
            self.user_cache = authenticate(username=username, password=password)
            if self.user_cache is None:
                raise forms.ValidationError("نام کاربری یا کلمه عبور اشتباه است.")
            elif not self.user_cache.is_active:
                raise forms.ValidationError("حساب کاربری بسته شده است.")

        else:
            raise forms.ValidationError('نام کاربری و کلمه عبور الزامی است.')

        return self.cleaned_data

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache


class Register(forms.Form):
    """
    Base class for authenticating users. Extend this to get a form that accepts
    username/password logins.
    """
    email = forms.CharField(label="email", widget=forms.EmailInput)
    username = forms.CharField(label="Username", max_length=30)
    password = forms.CharField(label="Password", widget=forms.PasswordInput)

    def clean(self):
        email = self.cleaned_data.get('email')
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        if username and password and email:
            username_check = User.objects.filter(username=username)
            email_check = User.objects.filter(email=email)
            if username_check.exists():
                raise forms.ValidationError("این نام کاربری قبلا ثبت شده است.")
            elif email_check.exists():
                raise forms.ValidationError("این ایمیل قبلا ثبت شده است.")
            else:
                return self.cleaned_data

        else:
            raise forms.ValidationError('ایمیل ,نام کاربری وکلمه عبور الزامی است.')


class UploadImageForm(forms.Form):
    image = forms.ImageField()


class UserInfoForm(forms.Form):
    first_name = forms.CharField(max_length=20, label='نام')
    last_name = forms.CharField(max_length=40, label='نام خانوادگی')
