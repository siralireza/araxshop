from django.contrib.auth.models import User
from django.db import models
from django.forms import ModelForm


class UserGallery(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_gallery')
    image = models.ImageField(upload_to='gallery/%Y/%m/%d/')
    size = models.CharField(max_length=10, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, blank=True, null=True)


# form
class UploadToGalleryForm(ModelForm):
    class Meta:
        model = UserGallery
        fields = ('image',)


class UserInformation(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='user_info')
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=50)
    user_type = models.CharField(max_length=10)
    # TODO More information should add here
    created_at = models.DateTimeField(auto_now_add=True, editable=False, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, blank=True, null=True)


class Ticketing(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_ticket')
    message = models.CharField(max_length=500)
    answer = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True, editable=False, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, blank=True, null=True)


class SendTicket(ModelForm):
    class Meta:
        model = Ticketing
        fields = ('message',)


class AdminSendTicket(ModelForm):
    class Meta:
        model = Ticketing
        exclude = ('created_at', 'updated_at')


class Products(models.Model):
    image = models.ImageField(upload_to='Product/%Y/%m/%d/', blank=True)
    name = models.CharField(max_length=20)
    code = models.CharField(max_length=40, blank=True, null=True)
    pic_number = models.PositiveSmallIntegerField(blank=True, null=True)
    pic_size = models.CharField(max_length=10, blank=True, null=True)
    group = models.CharField(max_length=50, blank=True, null=True)
    description = models.CharField(max_length=500, blank=True, null=True)
    price = models.IntegerField()
    exist = models.BooleanField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, blank=True, null=True)


class AddProduct(ModelForm):
    class Meta:
        model = Products
        exclude = ('created_at', 'updated_at')


class Factor(models.Model):
    product = models.ForeignKey(Products, on_delete=models.CASCADE, related_name='buy_product')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='buy_user')
    number = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True, editable=False, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, editable=False, blank=True, null=True)


class AddToFactor(ModelForm):
    class Meta:
        model = Factor
        fields = ('product','number')


class Payed(models.Model):
    factor = models.OneToOneField(User, on_delete=models.CASCADE, related_name='factor_paid')
    merchant_code = models.CharField(max_length=50)
    # todo later should complete this section
